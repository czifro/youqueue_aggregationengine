﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YouQueueAggregationEngine;

namespace EngineTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAMC()
        {
            var amc = new AMCScraper();

            var results = amc.GetOATShows();

            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void TestOmdb()
        {
            var omdb = new OmdbClient();

            var results = (Tuple<string, string[], string[], string, string>)omdb.GetShowData("Arrow");

            var s = results.Item1;

            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void TestEpGuides()
        {
            var epGuides = new EpGuidesClient();

            var results = epGuides.GetEpisodeSynopsis("Arrow", "The Offer");

            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void TestCW()
        {
            var cw = new CWScraper();
            var epGuides = new EpGuidesClient();
            var omdb = new OmdbClient();

            var results = cw.GetCurrentShows();

            var tuples = results[0];

            //tuples.RemoveAll(x => x.Item1 == "More Videos");

            var list = (List<object>)(from tuple in tuples where tuple.Item1 != "More Video"
                        let eps = cw.GetEpisodes(tuple.Item3)[0]
                        let meta = (Tuple<string, string[], string[], string, string>)omdb.GetShowData(tuple.Item1)
                        let episodes = (from ep in eps
                                        let show = (tuple.Item1 == "The Flash" ? "Flash_2014" : (tuple.Item1.Substring(0, ("The ").Length) == "The " ? tuple.Item1.Replace("The ", "") : tuple.Item1.Replace("?", "_US")))
                                        let s = epGuides.GetEpisodeSynopsis(show, ep.Item1)
                                        select new
                                        {
                                            name = ep.Item1,
                                            seasonNum = ep.Item2,
                                            episodeNum = ep.Item3,
                                            synopsis = (s != null ? s[0].Item1 : "Not Available"),
                                            imageUrl = ep.Item4,
                                            videoUrl = ep.Item5
                                        }).Cast<object>().ToList()
                        select new
                        {
                            name = tuple.Item1,
                            description = meta.Item5.Replace("\n", ""),
                            genres = meta.Item2.ToList().Select(x => x.Replace(" ", "")).ToArray(),
                            years = meta.Item3,
                            imageUrl = tuple.Item2,
                            rating = meta.Item4,
                            episodes = episodes
                        }).Cast<object>().ToList();

            var listString = new JavaScriptSerializer().Serialize(list);

            Assert.IsNotNull(results);
        }
    }
}
