﻿namespace YouQueueAggregationEngine
    module HttpUtility =
        open System
        open System.IO
        open System.Net

        let request (url:string) =
            let r = WebRequest.Create(url) :?> HttpWebRequest
            r.Method <- "GET"
            r

        let response (wr:WebRequest) = 
            let r = wr.GetResponse()
            let s = r.GetResponseStream()
            let sr = new StreamReader(s, System.Text.Encoding.UTF8)
            sr.ReadToEnd()