﻿namespace YouQueueAggregationEngine
    
        open System
        open System.Linq
        open YouQueueAggregationEngine.HttpUtility
        open YouQueueAggregationEngine.HAPHelper
    
        type public TNTScraper() =
            let baseUrl = "http://www.tntdrama.com/"
    
            member this.GetShowsAndEpisodes() =
                let showsUrl = baseUrl + "shows/index.html"
                let rawContents = response(request(showsUrl))
                createDoc rawContents
                |> descendants "div"
                |> Seq.map (fun div ->
                    let divClass = div |> attr "class"
                    let innerHtml = div.InnerHtml
                    innerHtml, divClass
                )
                |> Seq.filter (fun (_, divClass) -> divClass = "page maincarousel carousel-loading")
                |> Seq.map (fun (innerHtml, _) ->
                    createDoc innerHtml
                    |> elements "div"
                    |> Seq.filter (fun div -> div.Element("div").Element("p").FollowingSibling("p").InnerText <> "0 Episodes Available")
                    |> Seq.map (fun div ->
                        let rowHdrDiv = div.Element("div")
                        if rowHdrDiv.Element("p").FollowingSibling("p").InnerText <> "0 Episodes Available" then
                            let title = rowHdrDiv.Element("h2").Element("span").InnerText
                            let tvRating = rowHdrDiv.Element("p").Element("span").Element("span").InnerText
                            let showDetails = (title, tvRating)
                            let shows = 
                                div
                                |> elements "div"
                                |> Seq.map (fun subDiv ->
                                    let subDivClass = subDiv |> attr "class"
                                    let subDivInnerHtml = subDiv.InnerHtml
                                    subDivInnerHtml, subDivClass
                                )
                                |> Seq.filter (fun (_, subDivClass) -> subDivClass <> "row-hdr")
                                |> Seq.map (fun (subDivInnerHtml, _) ->
                                    let subDiv = createDoc subDivInnerHtml
                                    let availExpire = subDiv.Element("span").GetAttributeValue("availExpire", "null")
                                    let title = subDiv.Element("h2").Element("span").InnerText
                                    let imageUrl = subDiv.Element("img").GetAttributeValue("data-standard", "null")
                                    let seasonNode = subDiv.ChildNodes.SingleOrDefault(fun x -> x.GetAttributeValue("data-id", "notSeason") = "season")
                                    let seasonNum = if seasonNode <> null then seasonNode.Element("span").InnerText else ""
                                    let episode = subDiv.ChildNodes.SingleOrDefault(fun x -> x.GetAttributeValue("data-id", "notEp") = "ep")
                                    let episodeNum = if episode <> null then episode.Element("span").InnerText else ""
                                    let descNode = subDiv.ChildNodes.SingleOrDefault(fun x -> x.GetAttributeValue("data-id", "notEp-descr") = "ep-descr")
                                    let desc = if descNode <> null then descNode.Element("span").InnerText else ""
                                    let videoPath = subDiv.Element("a").GetAttributeValue("href", "null")
                                    title, seasonNum, episodeNum, desc, availExpire, imageUrl, videoPath
                                )
                                |> Seq.toArray
                            showDetails, shows
                        else
                            ("",""), null
                    )
                    |> Seq.toArray
                )
                |> Seq.toArray
