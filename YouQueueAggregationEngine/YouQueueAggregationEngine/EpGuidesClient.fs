﻿namespace YouQueueAggregationEngine
    
        open System
        open System.Linq
        open YouQueueAggregationEngine.HttpUtility
        open YouQueueAggregationEngine.HAPHelper

        type public EpGuidesClient() =
            let baseurl = "http://epguides.com/"

            member this.GetTvRageLink(show: string, episodeName: string) =
                let url = baseurl + show.Replace(" ", "")
                let rawContents = response(request(url))
                let tuples = createDoc rawContents
                             |> descendants "a"
                             |> Seq.map (fun a ->
                                 let href = a |> attr "href"
                                 let innerText = a |> innerText
                                 href, innerText
                             )
                             |> Seq.filter (fun (_, innerText) -> innerText.ToUpper() = episodeName.ToUpper())
                             |> Seq.toArray
                let (href, innerText) = if tuples.Count() > 0 then tuples.ElementAt(0) else ("","")

                href

            member this.GetEpisodeSynopsis(show: string, episodeName: string) =
                let url = this.GetTvRageLink(show, episodeName)
                if url = "" then
                    null
                else
                    let rawContents = response(request(url))
                    createDoc rawContents
                    |> descendants "div"
                    |> Seq.map (fun div ->
                        let divClass = div |> attr "class"
                        let innerText = div |> innerText
                        innerText, divClass
                    )
                    |> Seq.filter (fun (_, divClass) -> divClass = "show_synopsis")
                    |> Seq.toArray

