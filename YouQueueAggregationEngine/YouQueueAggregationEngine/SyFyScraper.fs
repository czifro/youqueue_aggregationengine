﻿namespace YouQueueAggregationEngine
    
        open System
        open System.Collections.Generic
        open System.Linq
        open YouQueueAggregationEngine.HttpUtility
        open YouQueueAggregationEngine.HAPHelper

        type public SyFyScraper() =
            let baseUrl = "http://www.syfy.com/shows"

            member this.GetShows() =
                let rawContents = response(request(baseUrl))
                createDoc rawContents
                |> descendants "div"
                |> Seq.map (fun div ->
                    let divClass = div.GetAttributeValue("class", "not_class")
                    let innerHtml = div.InnerHtml
                    innerHtml, divClass
                )
                |> Seq.filter (fun (_, divClass) -> divClass = "panel-pane pane-views-panes pane-all-shows-pane-all")
                |> Seq.map (fun (innerHtml, _) ->
                    createDoc innerHtml
                    |> descendants "div"
                    |> Seq.map (fun div2 ->
                        let divClass = div2.GetAttributeValue("class", "not_class")
                        let innerHtml = div2.InnerHtml
                        innerHtml, divClass
                    )
                    |> Seq.filter (fun (_, divClass) -> divClass = "view-content")
                    |> Seq.map (fun (innerHtml, _) ->
                        let html = createDoc innerHtml
                        let shows = 
                            html
                            |> elements "div"
                            |> Seq.map (fun div4 -> 
                                let div = div4
                                let lastChild = div4.LastChild.PreviousSibling
                                div, lastChild
                            )
                            |> Seq.filter (fun (_, lastChild) -> lastChild.Name = "a" && lastChild.InnerText = "Watch Full Episodes")
                            |> Seq.map (fun (div, _) ->
                                let imageUrl = div.Element("div").Element("a").Element("picture").Element("img").GetAttributeValue("srcset", "");
                                let title =  div.Element("div").FollowingSibling("div").InnerText
                                let path = div.Element("a").GetAttributeValue("href", "")
                                title, path, imageUrl
                            )
                            |> Seq.toArray

                        shows
                    )
                    |> Seq.toArray
                )
                |> Seq.toArray
