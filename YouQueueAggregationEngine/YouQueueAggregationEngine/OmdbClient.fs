﻿namespace YouQueueAggregationEngine

        open System
        open System.Linq
        open YouQueueAggregationEngine.HttpUtility
        open FSharp.Data

        type public OmdbClient() =
            let baseurl = "http://www.omdbapi.com/?"

            member this.GetShowData(show: string) =
                let url = baseurl + "t=" + show + "&plot=full&r=json"
                let json = response(request(url))
                let value = JsonValue.Parse(json)
                let genres = value.GetProperty("Genre").AsString().Split(',')
                let yearString = (value.GetProperty("Year").AsString() + "NULL")
                let years = yearString.Split(yearString.ElementAt(4))
                let desc = value.GetProperty("Plot").AsString()
                let rated = value.GetProperty("Rated").AsString()

                (show, genres, years, rated, desc)
                //0
