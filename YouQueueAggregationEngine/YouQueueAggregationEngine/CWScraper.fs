﻿namespace YouQueueAggregationEngine

    open System
    open System.Xml
    open System.Linq
    open YouQueueAggregationEngine.HttpUtility
    open YouQueueAggregationEngine.HAPHelper

    type public CWScraper() =
        let url = "https://www.cwtv.com"

        member this.GetCurrentShows() =
            let rawcontents = response(request(url + "/shows/"))
            createDoc rawcontents
            |> descendants "div"
            |> Seq.map (fun div ->
                let divClass = div.GetAttributeValue("class", null)
                let innerHtml = div.InnerHtml
                innerHtml, divClass
            )
            |> Seq.filter (fun (_,divClass) -> divClass = "shows-current")
            |> Seq.map (fun (innerHtml, divClass) -> 
                let html = createDoc innerHtml
                let ul = html.Element("ul")
                let shows =
                    ul
                    |> elements "li"
                    |> Seq.map (element "a")
                    |> Seq.map (fun a ->
                        let path = a |> attr "href"
                        let child = a.Element("div")
                        let img = child.Element("img")
                        let imageUrl = img.GetAttributeValue("src", null)
                        let title = child.FollowingSibling("p").InnerText
                        title, imageUrl, path
                    )
                    |> Seq.toArray
                shows
            )
            |> Seq.toArray

        member this.GetEpisodes(path: string) = 
            let showUrl = url + path
            let rawContents = response(request(showUrl))
            createDoc rawContents
            |> descendants "ul"
            |> Seq.map (fun ul -> 
                let ulClass = ul.GetAttributeValue("id", null)
                let innerHtml = ul.InnerHtml
                innerHtml, ulClass
            )
            |> Seq.filter (fun (_,ulClass) -> ulClass = "list_1")
            |> Seq.map (fun (innerHtml, ulClass) -> 
                let html = createDoc innerHtml
                //let div = html.Element("div").Element("div")
                let episodes =
                    html
                    |> elements "li"
                    |> Seq.map (element "div")
                    |> Seq.map (element "a")
                    |> Seq.map (fun a ->
                        let videoPath = a |> attr "href"
                        let firstDiv = a.Element("div")
                        let imageUrl = firstDiv.Element("img").GetAttributeValue("src", null)
                        let lastDiv = firstDiv |> followingSibling "div" |> followingSibling "div"
                        let details = lastDiv.Element("p").InnerText
                        let episodeName = details.Split('(').First().Trim()
                        let seasonNumber = details.Split('(').Last().Split('.').Last().Substring(0,1)
                        let episodeNumber = details.Split('(').Last().Split('.').Last().Replace(")", "").Substring(1)
                        //let date = lastDiv.Element("p").FollowingSibling("p").InnerText.Split(':').Last().Substring(1)
                        episodeName, seasonNumber, episodeNumber, imageUrl, (url + videoPath)
                    )
                    |> Seq.toArray
                episodes
            )
            |> Seq.toArray