﻿namespace YouQueueAggregationEngine

        open System
        open YouQueueAggregationEngine.HttpUtility
        open YouQueueAggregationEngine.HAPHelper
        
        type public AMCScraper() =
            let baseUrl = "http://www.amctv.com/#"

            member this.GetOATShows() =
                let rawContents = response(request(baseUrl))
                createDoc rawContents
                |> descendants "div"
                |> Seq.map (fun div ->
                    let divClass = div |> attr "class"
                    let innerHtml = div.InnerHtml
                    innerHtml, divClass
                )
                |> Seq.filter (fun (_, divClass) -> divClass = "menu-dropdowns-shows-on-air-container")
                |> Seq.map(fun (innerHtml, divClass) ->
                    let html = createDoc innerHtml
                    let ul = html.Element("ul")
                    let shows =
                        ul
                        |> elements "li"
                        |> Seq.map (element "a")
                        |> Seq.map (fun a ->
                            let title = a.InnerText
                            let url = a |> attr "href"
                            title, url
                        )
                        |> Seq.toArray
                    shows
                )
                |> Seq.toArray
